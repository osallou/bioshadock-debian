#!/bin/bash

apt-get update

apt-get install -y python grep-dctrl apt-file python-pip python-psycopg2 postgresql sudo wget

service postgresql start

WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -e $WORKDIR/udd.dump ]; then
    echo "Using existing udd.dump"
    cp $WORKDIR/udd.dump /tmp/udd.dump
else
    wget -O /tmp/udd.dump https://udd.debian.org/dumps/udd.dump
fi
SUDO="sudo -u postgres"
$SUDO createdb -T template0 -E SQL_ASCII udd
$SUDO pg_restore -j 8 -v -d udd /tmp/udd.dump
rm /tmp/udd.dump

$SUDO psql -c "ALTER USER postgres PASSWORD 'postgres';"

apt-file update

grep-aptavail --field=Maintainer 'debian-med-packaging@lists.alioth.debian.org' --show-field=Package,Version,Description > /bioshadock/list.txt

python /bioshadock/debian-biocontainers.py
