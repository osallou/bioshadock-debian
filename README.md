# About

Create Dockerfile and meta files from debianmed packages for bioshadock

Execute the following, with the argument begin the path to the docker output location:

    sudo bash debian.sh /home/osallou/Development/NOSAVE/bioshadock/debian/docker

    sudo bash biocontainer.sh /home/osallou/Development/NOSAVE/bioshadock/debian/biocontainers/containers

Provided Dockerfile is the base Debian image used to extract packages information.
