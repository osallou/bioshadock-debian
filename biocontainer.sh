#!/bin/bash

WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$1a" == "a" ]; then
    print "Missing docker directory"
    exit 1
fi

docker run -v $1:/bioconda --rm -it continuumio/miniconda bash -c "conda config --add channels bioconda && conda search -c bioconda --names-only > /bioconda/bioconda.txt "

docker pull debian:stable-backports
docker run --rm -v $WORKDIR:/bioshadock -v $1:/docker:rw debian:stable-backports /bioshadock/list_packages_biocontainer.sh

#curdir=${PWD}
#cd biocontainers/containers/debian
#git checkout master
#git pull origin master
#head_rev=`git show HEAD | sed -n 1p | cut -d " " -f 2`
#for package in ./*; do
#  git reset --hard -q $head_rev
#  pname=`basename $package`
#  echo "check $pname"
#  timestamp=$(date +%s)
#  #branch=biocontainer_${pname}_${timestamp}
#  #git checkout -b $branch
#
#  git add $name
#  git commit -m "create/update container $pname"
#  git push origin master


#  break
#done
