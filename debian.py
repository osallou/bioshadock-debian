import os
from subprocess import check_output
import logging
import base64
import urllib2
import psycopg2

class UDD(object):

    def __init__(self):
        self.packages = {}

    def load(self):
        conn = psycopg2.connect("host=localhost dbname=udd user=postgres password=postgres")
        cursor = conn.cursor()
        cursor.execute("SELECT * from edam")
        edams = cursor.fetchall()
        packages = {}
        for edam in edams:
            if edam[1] not in packages:
                packages[edam[1]] = {}
            packages[edam[1]]['topics'] = edam[2]
            packages[edam[1]]['scopes'] = edam[3]
        cursor.execute("SELECT * from bibref")
        bibrefs = cursor.fetchall()
        for bibref in bibrefs:
            if bibref[1] == 'doi':
                if bibref[3] not in packages:
                    packages[bibref[3]] = {}
                packages[bibref[3]]['doi'] = bibref[2]
        return packages

class Package(object):

    def __init__(self):
        self.name = None
        self.meta = {}

    def info(self):
        pass

stream = open('/bioshadock/list.txt','r')

packages = []

for line in iter(stream):
    if line.startswith('Package'):
        package = Package()
        package.name = line.split(':')[1].strip().replace("\n", "")
        packages.append(package)

stream.close()

udd = UDD()
udd_packages = udd.load()


for package in packages:
    if package.name.startswith('lib') or package.name.endswith('-doc') or package.name.endswith('-examples') or package.name.endswith('-dbg'):
        logging.debug('Skipping library '+package.name)
        continue
    out = check_output(["apt-cache", "show", package.name])
    lines = out.split("\n")
    package.tags = ""
    start_tag = False
    package.meta['name'] = package.name
    for line in lines:
        elts = line.split(':')
        value = ""
        if len(elts) > 1:
            key = elts[0]
            value = line.replace(key+':','').strip()
        if line.startswith('Version:'):
            package.meta['version'] = value
            continue
        if line.startswith('Description:'):
            package.meta['short_description'] = value
            continue
        if line.startswith('Homepage:'):
            package.meta['homepage'] = value
            continue
        if line.startswith('Tag:'):
            package.tags += value
            if line.endswith(','):
                start_tag = True
            continue
        if start_tag:
            package.tags += value
            if not line.endswith(','):
                start_tag = False
            continue
    package.meta['tags'] = package.tags.split(',')
    for tag in package.tags:
        tag = tag.strip()

    package.meta['copyright'] = None
    package.meta['license'] = None
    if 'version' in package.meta:
        # Try to fetch license
        try:
            response = urllib2.urlopen('http://metadata.ftp-master.debian.org/changelogs//main/'+package.name[0]+'/'+package.name+'/'+package.name+'_'+package.meta['version']+'_copyright')
            license_file = response.read().split("\n")
            license = None
            global_license = False
            for line in license_file:
                if line.startswith('Files: *'):
                    global_license = True
                    continue
                if global_license and line.startswith('Files:'):
                    global_license = False
                    break
                if global_license and line.startswith('Copyright:'):
                    package.meta['copyright'] = line.replace('Copyright:','').replace("'"," ").strip()
                if global_license and line.startswith('License:'):
                    package.meta['license'] = line.replace('License:','').strip()
        except Exception as e:
            logging.info('Could not fetch license info for '+package.name)
    out = check_output(["apt-file", "list", package.name])
    lines = out.split("\n")
    package_files = []
    for line in lines:
        if line:
            elts = line.split(':')
            package_file = elts[1].strip()
            if package_file.startswith('/usr/bin'):
                package_files.append(package_file)

    docker_dir = '/docker/debian/'
    package_name = package.name
    logging.warn('Generate Dockerfile '+str(docker_dir+package_name))
    if not os.path.exists(str(docker_dir+package_name)):
        os.makedirs(docker_dir+package_name)
    dockerfile_content = ''
    orig_b64 = None
    if os.path.exists(docker_dir+package_name+'/Dockerfile'):
        with open(docker_dir+package_name+'/Dockerfile', 'r') as content_file:
            dockerfile_orig_content = content_file.read()
        orig_b64 = base64.b64encode(dockerfile_orig_content)
    #dockerfile.write('FROM debian\n')
    dockerfile_content += 'FROM debian:stable-backports\n'
    #dockerfile.write('MAINTAINER bioshadock <support@genouest.org>\n')
    dockerfile_content += 'MAINTAINER bioshadock <support@genouest.org>\n'
    #dockerfile.write('LABEL')
    dockerfile_content += 'LABEL'
    tags = ''
    if 'short_description' in package.meta and  package.meta['short_description']:
        #dockerfile.write('    debian.description = "'+package.meta['short_description']+'" \\ \n')
        dockerfile_content += '    debian.description="'+package.meta['short_description']+'" \\ \n'
    if 'homepage' in package.meta and  package.meta['homepage']:
        #dockerfile.write('    debian.homepage = "'+package.meta['homepage']+'" \\ \n')
        dockerfile_content += '    debian.homepage="'+package.meta['homepage']+'" \\ \n'
    if 'version' in package.meta and  package.meta['version']:
        #dockerfile.write('    debian.version = "'+package.meta['version']+'" \\ \n')
        dockerfile_content += '    debian.version="'+package.meta['version']+'" \\ \n'
    if package.meta['copyright'] is not None:
        dockerfile_content += '    debian.copyright="'+package.meta['copyright']+'" \\ \n'
    if package.meta['license'] is not None:
        dockerfile_content += '    debian.license="'+package.meta['license']+'" \\ \n'
    if package_files:
        binaries = ",".join(package_files)
        dockerfile_content += '    debian.binaries="'+binaries+'" \\ \n'

    if package_name in udd_packages:
        if 'doi' in udd_packages[package_name]:
            dockerfile_content += '    debian.doi="' + str(udd_packages[package_name]['doi'])+'" \\ \n'
        if 'topics' in udd_packages[package_name]:
            dockerfile_content += '    debian.topics="' + str(udd_packages[package_name]['topics'])+'" \\ \n'
            package.meta['tags'] += udd_packages[package_name]['topics']
        if 'scopes' in udd_packages[package_name]:
            dockerfile_content += '    debian.scopes="' + str(udd_packages[package_name]['scopes'])+'" \\ \n'
            for scope in udd_packages[package_name]['scopes']:
                package.meta['tags'] += scope['function']
                if 'inputs' not in scope:
                    scope['inputs'] = []
                for scope_input in scope['inputs']:
                    if scope_input['data'] not in package.meta['tags']:
                        package.meta['tags'].append(scope_input['data'])
                    for scope_input_format in scope_input['formats']:
                        if scope_input_format not in package.meta['tags']:
                            package.meta['tags'].append(scope_input_format)
                if 'outputs' not in scope:
                    scope['outputs'] = []
                for scope_output in scope['outputs']:
                    if scope_output['data'] not in package.meta['tags']:
                        package.meta['tags'].append(scope_output['data'])
                    for scope_output_format in scope_output['formats']:
                        if scope_output_format not in package.meta['tags']:
                            package.meta['tags'].append(scope_output_format)

    if 'tags' in package.meta and package.meta['tags']:
        tags = ",".join(package.meta['tags'])
    if tags:
        #dockerfile.write('    debian.tags = "'+tags+'" \\ \n')
        dockerfile_content += '    debian.tags="'+str(tags)+'" \\ \n'

    #dockerfile.write('    bioshadock.Vendor="bioshadock"\n\n')
    dockerfile_content += '    bioshadock.Vendor="bioshadock"\n\n'
    dockerfile_content += 'ENV DEBIAN_FRONTEND noninteractive\n'
    #dockerfile.write('RUN ["apt-get", "update"]\n')
    dockerfile_content += 'RUN ["apt-get", "update"]\n'
    #dockerfile.write('RUN ["apt-get", "install", "-y", "'+package.name+'"]\n')
    dockerfile_content += 'RUN ["apt-get", "install", "-y", "'+package.name+'"]\n'
    new_b64 = base64.b64encode(dockerfile_content)
    if orig_b64 is None or orig_b64 != new_b64:
        dockerfile = open(docker_dir+package_name+'/Dockerfile', 'w')
        dockerfile.write(dockerfile_content)
        dockerfile.close()
        dockernew = open(docker_dir+package_name+'/docker.new', 'w')
        dockernew.write(package.meta['version'])
        dockernew.close()
        with open(docker_dir + package_name + '/package.yaml', 'w') as f:
            f.write('package:\n')
            f.write('    name: \''+package.name+'\'\n')
            f.write('    container: \'debian/'+package.name.replace('.', '-')+'\'\n')
            if 'version' in package.meta and  package.meta['version']:
                f.write('    version: \''+package.meta['version']+'\'\n')
            summary = package.name
            if 'short_description' in package.meta and  package.meta['short_description']:
                summary = package.meta['short_description'].replace("'"," ")
            f.write('about:\n')
            f.write('    summary: \''+summary+'\'\n')
            if package.meta['copyright'] is not None:
                f.write('    copyright: \''+package.meta['copyright']+'\'\n')
            if package.meta['license'] is not None:
                f.write('    license: \''+package.meta['license']+'\'\n')
    else:
        logging.debug('Skipping '+package.name+', same as previous')
