#!/bin/bash

WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$1a" == "a" ]; then
    print "Missing docker directory"
    exit 1
fi

docker run --rm -v $WORKDIR:/bioshadock -v $1:/docker:rw debian:stable-backports /bioshadock/list_packages.sh
