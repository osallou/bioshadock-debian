import os
from subprocess import check_output
import logging
import base64
import urllib2
import psycopg2
import re

class UDD(object):

    def __init__(self):
        self.packages = {}

    def load(self):
        conn = psycopg2.connect("host=localhost dbname=udd user=postgres password=postgres")
        cursor = conn.cursor()
        cursor.execute("SELECT * from edam")
        edams = cursor.fetchall()
        packages = {}
        for edam in edams:
            if edam[1] not in packages:
                packages[edam[1]] = {}
            packages[edam[1]]['topics'] = edam[2]
            packages[edam[1]]['scopes'] = edam[3]
        cursor.execute("SELECT * from bibref")
        bibrefs = cursor.fetchall()
        for bibref in bibrefs:
            if bibref[1] == 'doi':
                if bibref[3] not in packages:
                    packages[bibref[3]] = {}
                packages[bibref[3]]['doi'] = bibref[2]
        return packages

class Package(object):

    def __init__(self):
        self.name = None
        self.meta = {}

    def info(self):
        pass

bioconda_packages = []
with open('/docker/bioconda.txt', 'r') as f:
    for line in f:
        bioconda_packages.append(line.strip().replace("\n", ""))


stream = open('/bioshadock/list.txt','r')

packages = []

for line in iter(stream):
    if line.startswith('Package'):
        package = Package()
        package.name = line.split(':')[1].strip().replace("\n", "")
        packages.append(package)

stream.close()

udd = UDD()
udd_packages = udd.load()

blacklist = [
             'freemedforms-common-resources', 'freemedforms-emr', 'freemedforms-emr-doc-en',
             'freemedforms-emr-doc-fr', 'freemedforms-emr-resources', 'freemedforms-freedata',
             'freemedforms-i18n', 'freemedforms-libs', 'freemedforms-project', 'freemedforms-theme'
            ]


for package in packages:
    if package.name.startswith('r-cran') or package.name.startswith('fonts-') or package.name in blacklist:
        logging.debug('Skipping library '+package.name)
        continue
    if package.name.startswith('lib') or package.name.endswith('-doc') or package.name.endswith('-examples') or package.name.endswith('-dbg') or package.name.startswith('med-'):
        logging.debug('Skipping library '+package.name)
        continue

    out = check_output(["apt-cache", "show", package.name])
    lines = out.split("\n")
    package.tags = ""
    start_tag = False
    package.meta['name'] = package.name
    for line in lines:
        elts = line.split(':')
        value = ""
        if len(elts) > 1:
            key = elts[0]
            value = line.replace(key+':','').strip()
        if line.startswith('Source:'):
            package.meta['deb_source'] = value
        if line.startswith('Version:'):
            package.meta['version'] = value
            # sed -rne 's/^Version: ([0-9.]+)[-+].*$$/\1/p'
            res = re.match(r'([0-9.]+)[-+].*$', value)
            package.meta['upstream_version'] = None
            if res and len(res.groups()) == 1:
                package.meta['upstream_version'] = res.groups()[0]
            continue
        if line.startswith('Description:'):
            package.meta['short_description'] = value.replace('"','')
            continue
        if line.startswith('Homepage:'):
            package.meta['homepage'] = value
            continue
        if line.startswith('Tag:'):
            package.tags += value
            if line.endswith(','):
                start_tag = True
            continue
        if start_tag:
            package.tags += value
            if not line.endswith(','):
                start_tag = False
            continue

    if 'deb_source' not in package.meta:
        package.meta['deb_source'] = package.name
    package.meta['tags'] = package.tags.split(',')
    for tag in package.tags:
        tag = tag.strip()

    is_conda = False
    package.meta['pkg_name'] = package.name
    package.meta['conda'] = None
    in_conda = 0
    for bioconda_package in bioconda_packages:
        rewrited_name = package.name.replace('python-','').replace('python3-','')
        if rewrited_name.startswith('r-bioc'):
            rewrited_name = rewrited_name.replace('r-bioc', 'bioconductor')
            package.name = rewrited_name
        if rewrited_name == bioconda_package or package.meta['deb_source'] == bioconda_package:
            logging.warn('Bioconda package %s already exists' % (bioconda_package))
            in_conda += 1
            is_conda = True
            package.meta['conda'] = rewrited_name
            break
        if package.name in bioconda_package or bioconda_package in package.name:
            logging.warn('Bioconda package may match: %s vs %s' % (package.name, bioconda_package))
        elif package.meta['deb_source'] in bioconda_package or bioconda_package in package.meta['deb_source']:
            logging.warn('Bioconda package may match: %s vs %s' % (package.meta['deb_source'], bioconda_package))
    logging.warn('Found %d packages in bioconda' % (in_conda))

    try:
        response = urllib2.urlopen('https://bio.tools/api/tool/' + str(package.meta['deb_source']) + '/?format=json')
        if response.getcode() == 200:
            package.meta['biotools'] = str(package.meta['deb_source'])
    except Exception as e:
        logging.debug('no biotools found')

    package.meta['copyright'] = None
    package.meta['license'] = None

    if 'version' in package.meta:
        # Try to fetch license
        try:
            v = package.meta['version']
            # Case of binary nmu
            if '+b' in v:
                tmpv = v.split('+b')
                v = tmpv[0]
            response = urllib2.urlopen('http://metadata.ftp-master.debian.org/changelogs/main/' + package.meta['deb_source'][0] + '/' + package.meta['deb_source'] + '/stable_copyright')
            license_file = response.read().split("\n")
            license = None
            global_license = False
            for line in license_file:
                if line.startswith('Files: *'):
                    global_license = True
                    continue
                if global_license and line.startswith('Files:'):
                    global_license = False
                    break
                if global_license and line.startswith('Copyright:'):
                    package.meta['copyright'] = line.replace('Copyright:','').replace("'"," ").replace('"','').strip()
                if global_license and line.startswith('License:'):
                    package.meta['license'] = line.replace('License:','').strip()
        except Exception as e:
            logging.warn('Could not fetch license info for '+package.name+', '+str(e))

    out = check_output(["apt-file", "list", package.meta['pkg_name']])
    lines = out.split("\n")
    package_files = []
    for line in lines:
        if line:
            elts = line.split(':')
            package_file = elts[1].strip()
            if package_file.startswith('/usr/bin'):
                package_files.append(package_file)

    docker_dir = '/docker/'

    package.meta['version'] += '-deb'
    if package.name.startswith('python-'):
        package.name = package.name.replace('python-','')
        package.meta['version'] += '-py2'
    if package.name.startswith('python3-'):
        package.name = package.name.replace('python3-','')
        package.meta['version'] += '-py3'

    package_name = package.name.replace('+','-plus')

    logging.info('Generate Dockerfile '+str(docker_dir+package_name+'/'+package.meta['version']))
    dockerfile_content = ''
    orig_b64 = None
    if os.path.exists(docker_dir+package_name+'/'+package.meta['version'].replace('-deb','')+'/Dockerfile'):
        logging.info('Non debian version already exists, skipping')
        continue
    if package.meta['upstream_version'] and os.path.exists(docker_dir+package_name+'/'+package.meta['upstream_version']+'/Dockerfile'):
        logging.info('Non debian version already exists, skipping')
        continue   
    package.meta['version'] = package.meta['version'].replace('+','').replace('~','').replace(':','-')

    if not os.path.exists(str(docker_dir+package_name+'/'+package.meta['version'])):
        os.makedirs(docker_dir+package_name+'/'+package.meta['version'])
    if os.path.exists(docker_dir+package_name+'/'+package.meta['version']+'/Dockerfile'):
        with open(docker_dir+package_name+'/'+package.meta['version']+'/Dockerfile', 'r') as content_file:
            dockerfile_orig_content = content_file.read()
        orig_b64 = base64.b64encode(dockerfile_orig_content)
    #dockerfile_content += 'FROM debian:stable-backports\n'
    dockerfile_content += 'FROM biocontainers/biocontainers:debian-stretch-backports\n'
    dockerfile_content += 'MAINTAINER biocontainers <biodocker@gmail.com>\n'
    dockerfile_content += 'LABEL'
    tags = ''
    updated_package_name = package_name
    dockerfile_content += '    software="%s" \\ \n' % (package_name)
    pattern=re.compile("^([a-zA-Z0-9_-])+$")
    if pattern.match(updated_package_name) is None:
        logging.error(package_name + " has invalid name: " + str(updated_package_name))
    dockerfile_content += '    container="%s" \\ \n' % (package_name)
    if 'short_description' in package.meta and  package.meta['short_description']:
        #dockerfile.write('    debian.description = "'+package.meta['short_description']+'" \\ \n')
        dockerfile_content += '    about.summary="'+package.meta['short_description']+'" \\ \n'
    if 'homepage' in package.meta and  package.meta['homepage']:
        #dockerfile.write('    debian.homepage = "'+package.meta['homepage']+'" \\ \n')
        dockerfile_content += '    about.home="'+package.meta['homepage']+'" \\ \n'
    if 'version' in package.meta and  package.meta['version']:
        #dockerfile.write('    debian.version = "'+package.meta['version']+'" \\ \n')
        pattern=re.compile("^([a-zA-Z0-9_-])+$")
        if pattern.match(package.meta['version'].replace('.','').replace('~','-').replace(':', '-')) is None:
            logging.error(package_name + " has invalid version: " + str(package.meta['version']))
        dockerfile_content += '    software.version="'+ package.meta['version'] +'" \\ \n'
        dockerfile_content += '    version="1" \\ \n'

    if 'biotools' in package.meta and package.meta['biotools']:
        dockerfile_content += '    extra.identifiers.biotools="'+ package.meta['biotools'] + '" \\ \n'

    if package.meta['copyright'] is not None:
        dockerfile_content += '    about.copyright="'+package.meta['copyright']+'" \\ \n'
    if package.meta['license'] is not None:
        dockerfile_content += '    about.license="'+package.meta['license']+'" \\ \n'
    dockerfile_content += '    about.license_file="/usr/share/doc/'+package_name+'/copyright" \\ \n'
    if package_files:
        binaries = ",".join(package_files)
        dockerfile_content += '    extra.binaries="'+binaries+'" \\ \n'
    if package_name in udd_packages:
        if 'doi' in udd_packages[package_name]:
            dockerfile_content += '    extra.identifiers.doi="' + str(udd_packages[package_name]['doi'])+'" \\ \n'
        if package.meta['conda']:
            dockerfile_content += '    extra.identifiers.conda="' + str(package.meta['conda'])+'" \\ \n'

    if 'tags' in package.meta and package.meta['tags']:
        tags = ",".join(package.meta['tags'])
    if tags:
        dockerfile_content += '    about.tags="'+str(tags)+'" \n\n'
    else:
        dockerfile_content += '    about.tags=""\n\n'


    dockerfile_content += 'ENV DEBIAN_FRONTEND noninteractive\n'
    # dockerfile_content += 'RUN useradd biodocker\n'
    dockerfile_content += 'RUN apt-get update && apt-get install -y ' + package.meta['pkg_name'] + ' && apt-get clean && apt-get purge && rm -rf /var/lib/apt/lists/* /tmp/*\n'
    dockerfile_content += 'USER biodocker\n'
    new_b64 = base64.b64encode(dockerfile_content)
    if orig_b64 is None or orig_b64 != new_b64:
        dockerfile = open(docker_dir+package_name+'/'+package.meta['version']+'/Dockerfile', 'w')
        dockerfile_content = re.sub(r'[^\x00-\x7f]',r'', dockerfile_content)
        dockerfile.write(dockerfile_content)
        dockerfile.close()
    else:
        logging.debug('Skipping '+package.name+', same as previous')
